let ircServer = require("./IRC-Server/IrcServer.js");
let comandos = require("./IRC-Server/comandos/comandos.js");
let net = require('net');
let stalkerServiceStarted = false;

function startServiceStalker() {
	if (!stalkerServiceStarted) {
		let stalker = require("./IRC-Server/utils/stalker");
		setInterval(function () {
			stalker.stalkear(ircServer);
		}, ircServer.getPingInterval())

	}
	stalkerServiceStarted = true;
}


net.createServer(function (socket) {
	startServiceStalker();
	socket.name = socket.remoteAddress + ":" + socket.remotePort;
	ircServer.addSocket(socket);
	socket.on('data', function (data) {
		comandos.execute(data, socket, ircServer);
	});

	socket.on('end', function () {
		ircServer.removeSocket(socket);
	})

}).listen(ircServer.getPort());

console.log("Servidor IRC executando na Porta: " + ircServer.getPort() + "\n");
