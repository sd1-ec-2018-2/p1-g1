# PROJETO 1 DA DISCIPLINA DE APLICAÇÕES DISTRIBUÍDAS

## Membros do Projeto "P1-G1"


 - Amanda da Cruz Miranda - amandaamiranda@hotmail.com - Líder
 - Daniel Rodrigues dos Santos - danielnd14@gmail.com - Desenvolvedor
 - Kamila Cristina Silva Martins - kcrissmartins@gmail.com - Tester
 - Márcio Rodrigues Filho - rodriguesfilhomarcio@gmail.com -  Desenvolvedor

## Documentações
[Organização Estrutural do Projeto](https://gitlab.com/sd1-ec-2018-2/p1-g1/wikis/Organiza%C3%A7%C3%A3o-do-projeto)

[Casos de Testes](https://gitlab.com/sd1-ec-2018-2/p1-g1/wikis/Documenta%C3%A7%C3%B5es-de-testes)

[Configurações do servidor](https://gitlab.com/sd1-ec-2018-2/p1-g1/wikis/Configura%C3%A7%C3%B5es)