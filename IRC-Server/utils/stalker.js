let ping = require("../comandos/executaveis/ping");
let inativeClients = [];

function stalkear(ircServer) {

	if (inativeClients.length > 0) {
		let clientsSemResposta = ircServer.getInativeClients()
			.filter(function (socket) {
				return inativeClients.includes(socket);
			});
		ircServer.quitCompulsorio(clientsSemResposta);
	}

	inativeClients = ircServer.getInativeClients();
	inativeClients.forEach(function (socket) {
		let args = ["ping", socket.nick];
		ping.execute(args, socket, ircServer)
	})

}

let stalker = {
	stalkear: stalkear
};

module.exports = stalker;
