module.exports = class Channel {
	constructor(name, creator, key, maxClients) {
		this.name = name;
		this.users = [creator];
		this.topic = '';
		this.creator = creator;
		this.operators = [];
		this.voicers = [];
		this.modes = ['t'];
		this.maxClients = maxClients;
		this.invited = [];
		this.key = key ? key.trim() : "";
	}

	isOperator(socket) {
		return this.creator === socket || this.operators.includes(socket)
	}

	isVoicer(socket) {
		return this.isOperator(socket) || this.voicers.includes(socket)
	}

	isModerate() {
		return this.modes.includes("m");
	}

	isMember(socket) {
		return this.users.includes(socket);
	}

	isInvited(socket) {
		return this.invited.includes(socket);
	}

	isInvitedOnly() {
		return this.modes.includes("i")
	}

	addMember(socket, key) {
		if (this.isMember(socket)) {
			sendFeedBack(socket, this.name, "ERRO: Esse usuário já é membro do canal");
			return
		}
		if ((this.users.length + this.invited.length) === this.maxClients) {
			sendFeedBack(socket, this.name, "Canal lotado!");
			return
		}
		if (key !== this.key) return sendFeedBack(socket, this.name, "Senha invalida");
		this.users.push(socket);

		this.sendMessage(socket, "<" + socket.nick + "> : acabou de se juntar ao canal");
	}

	addInvited(socket) {
		this.invited.push(socket)
	}

	addOperator(socket) {
		this.operators.push(socket)
	}

	addAllMembers(listSockets) {
		this.users = this.users.concat(listSockets)
	}

	removeAllInvited() {
		this.invited = [];
	}


	rmOperator(socket) {
		this.operators.splice(this.operators.indexOf(socket), 1)
	}

	rmMember(socket) {
		this.users.splice(this.users.indexOf(socket), 1);
	}

	setTopic(topic) {
		this.topic = topic;
	}

	setMaxClients(max) {
		this.maxClients = max;
	}

	addVoicer(socket) {
		this.voicers.push(socket)
	}

	rmVoicer(socket) {
		this.voicers.splice(this.voicers.indexOf(socket), 1);
	}

	sendMessage(fromSocket, message) {
		let name = this.name;
		if (this.isModerate() && !this.isVoicer(fromSocket)) {
			sendFeedBack(fromSocket, name, "Canal moderado, portanto somente voicers podem falar");
			return
		}
		this.users.forEach(function (socket) {
			if (socket === fromSocket)
				return;
			let msg = "[" + name + "] " + message + "\n";
			socket.write(msg)
		})
	}

};

function sendFeedBack(socket, channelName, msg) {
	socket.write("<" + channelName + "> :" + msg + "\n");
}