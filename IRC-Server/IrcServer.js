let sockets = [];
let configs = require("./configs");
let uptime = Date.now();
let blackListNick = [];
let ircServer = {
	channels: [],
	uptime: uptime,
	addNickOnBlackList: function (nick) {
		blackListNick.push(nick)
	},

	isBlockedNick: function (nick) {
		return blackListNick.includes(nick)
	},

	getTotalClients: function () {
		return sockets.length
	},

	getPassword: function () {
		return configs.senhaAcesso
	},

	getMotd: function () {
		return configs.motd
	},

	getOperadoresPersistidos: function () {
		return configs.operadores
	},

	hasPassword: function () {
		return configs.senhaAcesso !== "" && configs.senhaAcesso !== undefined
	},

	getPort: function () {
		return configs.port
	},

	addSocket: function (socket) {
		sockets.push(socket)
	},

	removeSocket: function (socket) {
		sockets.splice(sockets.indexOf(socket), 1);
	},

	quitCompulsorio: quitCompulsorio,

	getAllClients: function () {
		return sockets.filter(function (skt) {
			return skt.identifier !== undefined
		})
	},

	findServerOperators: function () {
		return this.getAllClients().filter(function (socket) {
			return socket.userMode.includes("o")
		})
	},

	isServerOperator: function (socket) {
		let serverOperators = this.findServerOperators();
		return serverOperators.includes(socket);
	},

	findSocketByNick: function (nick) {
		let nickTratado = nick.trim()
		return sockets.find(socket => isStringEquals(socket.nick, nickTratado, true));
	},

	findSocketByUserName: function (userName) {
		let userNameTratado = userName.trim()
		return sockets.find(socket => isStringEquals(socket.user, userNameTratado, true));
	},

	findChannelByName: function (nome) {
		let nomeCanal = nome.trim();
		return this.channels.find(c => isStringEquals(c.name, nomeCanal, true))
	},

	findAllChannelsOfNick: function (nick) {
		let socketFinded = this.findSocketByNick(nick.trim());
		return this.channels.filter(function (channel) {
			return channel.isMember(socketFinded) || channel.isInvited(socketFinded)
		})
	},

	addChannel: function (channel) {
		if (!channel)
			return;

		if (!channel.name || !channel.users)
			return;

		this.channels.push(channel)
	},

	broadcast: function (message, fromSocket) {
		sendMsgToList(message, this.getAllClients(), fromSocket, true)
	},
	getMoment: getMoment,
	sendMessageBack: function (toSocket, message) {
		message = "<Servidor> : " + message
		sendMsgToList(message, [toSocket], toSocket, false)
	},

	isUserVisible: function (socket) {
		return socket.identifier && !socket.userMode.includes("i");
	},

	getInativeClients: function () {
		let clients = this.getAllClients();
		let time = Date.now();
		return clients.filter(function (socket) {
			return (Math.floor((time - socket.ultmaInteracao) / 1000)) > configs.pingTimeOut
		})
	},
	getPingInterval: function () {
		return configs.pingTimeOut * 1000;
	},
	estatisticas: {},

	getVersion: function () {
		return "Versão beta";
	}
};

function isStringEquals(string1, string2, caseInsensitive) {

	if (!string1 || !string2) {
		return false
	}
	if (caseInsensitive === true) {
		return string1.toLowerCase() === string2.toLowerCase();
	} else {
		return string1 === string2;
	}
}

function getMoment() {

	let dt = new Date();
	let hora = dt.getHours() < 10 ? "0" + dt.getHours() : dt.getHours();
	let minutos = dt.getMinutes() < 10 ? "0" + dt.getMinutes() : dt.getMinutes();
	let segundos = dt.getSeconds() < 10 ? "0" + dt.getSeconds() : dt.getSeconds();
	return "[" + hora + ":" + minutos + ":" + segundos + "] ";
}

function sendMsgToList(message, listSockets, fromSocket, evitarEco) {

	message = getMoment() + message;
	let messageFormatada = message.replace(/\s{2,}/g, " ") + "\n";
	listSockets.forEach(function (socket) {
		if (fromSocket === socket && evitarEco) {
			return
		}
		socket.write(messageFormatada)
	})
}

function quitCompulsorio(listSockets) {

	let args = ["quit", ":Desconectado pelo servidor"];
	let quit = require("./comandos/executaveis/quit");
	listSockets.forEach(function (socket) {
		ircServer.sendMessageBack(socket, "O servidor te desconectou automaticamente devido a sua ociosidade");
		ircServer.removeSocket(socket);
		quit.execute(args, socket, ircServer);
	})
}

module.exports = ircServer;