let comandoPassado;
module.exports = new function () {
	this.command = "who";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer)) return;

	let comandHelper = require("./comandHelper.js")
	let socketsDesconhecidos;
	if (!args[1] || args[1] == 0) {//pior caso
		socketsDesconhecidos = comandHelper.obtenhaTodosSocketsDesconhecidos(ircServer, socket)
		printUsers(socketsDesconhecidos, socket, "Todos Usuários", ircServer);
		return
	}

	let channels = comandHelper.findChannelsByMask(ircServer, args[1]);

	if (channels.length > 0) {
		let canaisExecutante = ircServer.findAllChannelsOfNick(socket.nick)
			.map(function (channel) {
				return channel.name;
			});

		channels.forEach(function (channel) {
			if (canaisExecutante.includes(channel.name)) {
				return;
			}
			printUsers(channel.users, socket, channel.name, ircServer);
		});

		return
	}

	socketsDesconhecidos = comandHelper.obtenhaSocketsDesconhecidosPorMascara(ircServer, socket, args[1]);

	printUsers(socketsDesconhecidos, socket, "Usuarios encontrados", ircServer)

}

function printUsers(listSockets, socketOrigem, titulo, ircServer) {

	if (listSockets.length < 1) {
		let message = "Nenhum alvo encontrado com a mascara passada!";
		ircServer.sendMessageBack(socketOrigem, message);
		return
	}

	let msgBody = listSockets.reduce(function (acumulator, socket) {
		return acumulator + "\t" + socket.nick + " | " + socket.realName + " | " + socket.identifier + "\n"
	}, "");
	ircServer.sendMessageBack(socketOrigem, titulo);
	socketOrigem.write(msgBody);
	ircServer.sendMessageBack(socketOrigem, "Fim da lista WHO");
}