let comandoPassado;
module.exports = new function () {
	this.command = "whois"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiUmArgumento(socket, args, ircServer))
		return;


	let comandHelper = require("./comandHelper.js");
	let mascara = args[1].trim();

	if (mascara) {
		let socketsDesconhecidos = comandHelper.findVisibleSocketsByMask(ircServer, socket, mascara);

		if (socketsDesconhecidos.length > 0) {
			socketsDesconhecidos.forEach(function (socketDesconhecido) {
				printUserInfo(socket, socketDesconhecido, ircServer)
			})
		} else {
			let message = "ERRO: Usuário não encontrado ou o nickname passado e o seu proprio";
			ircServer.sendMessageBack(socket, message)
		}
	}
}

function printUserInfo(socket, fromSocket, ircServer) {
	if (fromSocket && ircServer.isUserVisible(fromSocket)) {
		ircServer.sendMessageBack(socket, "Inicio lista Whois:");
		let nickname = fromSocket.nick;
		let realName = fromSocket.realName;
		let canais = ircServer.findAllChannelsOfNick(nickname)
		socket.write("\tNome: " + realName + "\n");

		if (canais.length) {
			let nomesCanais = canais.reduce(function (ant, canal) {
				return ant + "\t\t" + canal.name + "\n"
			}, "");
			socket.write("\tEstá nos canais:\n" + nomesCanais + "\n")
		}

		socket.write("\tEntrou no servidor em " + new Date(fromSocket.joined) + "\n");
		socket.write("\tInativo a " + (Math.floor((Date.now() - fromSocket.ultmaInteracao) / 1000)) + " segundos\n")
		ircServer.sendMessageBack(socket, "Fim lista Whois:");
	}
}
