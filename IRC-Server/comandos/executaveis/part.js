let comandoPassado;
module.exports = new function () {
	this.command = "part";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {

	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiUmArgumento(socket, args, ircServer))
		return;

	let canais = args[1].trim().split(",");
	let msg = args.join(' ').split(':')[1];

	if (canais[0] === "*") {
		let channels = ircServer.findAllChannelsOfNick(socket.nick)
		channels.forEach(function (canal) {
			leaveChannel(canal, ircServer, msg, socket)
		});
		return
	}

	if (canais.length === 1) {
		let channel = ircServer.findChannelByName(canais[0])
		leaveChannel(channel, ircServer, msg, socket)
		return
	}

	canais.forEach(function (nomeCanal) {
		if (!nomeCanal)
			return;
		let channel = ircServer.findChannelByName(nomeCanal);
		leaveChannel(channel, ircServer, msg, socket)
	})

}

function leaveChannel(channel, ircServer, msg, socket) {
	if (channel) {
		let strmsg = formatarMsg(channel.name, msg, socket);
		channel.rmMember(socket);
		channel.sendMessage(socket, strmsg)
	} else {
		let message = "ERRO: Canal inexistente!";
		ircServer.sendMessageBack(socket, message)
	}
}

function formatarMsg(nomeCanal, strMsg, socket) {
	let rgx = RegExp("\\[kicked\\]", "i");
	let exited = rgx.exec(strMsg) ? "foi expulso" : "saiu";

	if (strMsg) {
		strMsg = "O usuário <" + socket.nick + "> " + exited + " com a mensagem: " + strMsg.replace("[kicked]", "");
	} else {
		strMsg = "O usuário <" + socket.nick + "> " + exited
	}
	return strMsg
}
