module.exports = new function () {
	this.command = ""
	this.executed = false
	this.execute = function (args, socket, ircServer) {
		run(args, socket, ircServer)
	}
}

function run(args, socket, ircServer) {
	let message = "ERRO: comando " + args[0] + " suportado!"
	ircServer.sendMessageBack(socket, message)
	this.executed = true
}
