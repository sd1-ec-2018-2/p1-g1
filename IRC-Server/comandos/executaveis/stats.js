let comandoPassado;
module.exports = new function () {
	this.command = "stats";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer)
		|| !validacoes.possuiUmArgumento(socket, args, ircServer)) return;

	if (!socket.userMode.includes("o")) {
		let message = "ERRO: você nao possui permissao para executar esse comando!\n";
		ircServer.sendMessageBack(socket, message);
		return
	}

	let estatisticas = ircServer.estatisticas;

	switch (args[1].trim().toLocaleLowerCase()) {
		case "l":
			ircServer.getAllClients().forEach(function (client) {
				socket.write(`${client.name} - Bytes recebidos: ${client.bytesRead} / Bytes enviados: ${client.bytesWritten} - Tempo aberto: ${Math.floor((Date.now() - client.joined) / 1000)} segundos\r\n`);
			});
			break;
		case "m":
			let msg = "Comandos executados:\n";
			let comandosExecutados = estatisticas["comandosExecutados"];

			for (let comando in comandosExecutados) {
				msg += `\t${comando}: ${comandosExecutados[comando]}\n`;
			}

			socket.write(msg);
			break;
		case "o":
			let operators = ircServer.findServerOperators();
			let operadores = operators.reduce(function (acumulator, socket) {
				return acumulator + socket.user + ", "
			}, "");
			socket.write(`operador (es): ${operadores}\n`);
			break;
		case "u":
			let uptime = Math.floor((Date.now() - ircServer.uptime) / 1000);
			socket.write(`Servidor ligado por ${Math.floor(uptime / 86400)} dias, ${Math.floor((uptime % 86400) / 3600)} horas, ${Math.floor((uptime % 3600) / 60)} minutos e ${uptime % 60} segundos\r\n`);
			break;
		default:
			return socket.write(`ERRO: Argumento inválido!\n`);

	}

	socket.write("Fim da lista stats\n")

}
