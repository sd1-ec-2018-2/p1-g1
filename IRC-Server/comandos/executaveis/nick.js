let comandoPassado;

module.exports = new function () {
	this.command = "nick";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.possuiUmArgumento(socket, args, ircServer) ||
		!validacoes.isValidNickName(socket, args[1], ircServer))
		return;

	if (ircServer.isBlockedNick(args[1]))
		return ircServer.sendMessageBack(socket, "Voce foi bloqueado");

	if (ircServer.hasPassword() && !socket.logged) {
		ircServer.sendMessageBack(socket, "ERRO: O sevidor possui senha, use o comando pass <senha>");
		return
	}

	if (ircServer.findSocketByNick(args[1])) {
		let message = "ERRO: Já existe esse nickname!";
		ircServer.sendMessageBack(socket, message)
	} else {
		socket.nick = args[1].trim();
	}
}

