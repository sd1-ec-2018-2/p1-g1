let comandoPassado
module.exports = new function () {
	this.command = "list";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	if (ircServer.channels.length === 0) {
		let message = "Não existem canais abertos no momento!"
		ircServer.sendMessageBack(socket, message);
		return
	}
	else if (args.length === 1) {
		ircServer.sendMessageBack(socket, "Canais abertos:  ");
		ircServer.channels.forEach(function (channel) {
			printChannelInfo(channel, socket)
		});
	}
	else if (args.length === 2) {
		ircServer.sendMessageBack(socket, "Status: ");
		let parametros = args[1].split(',');
		parametros.forEach(function (parametro) {
			let channel = ircServer.findChannelByName(parametro);
			printChannelInfo(channel, socket)
		})
	}
	ircServer.sendMessageBack(socket, "Fim da lista")
}

function printChannelInfo(channel, socket) {
	if (channel) {
		let linha = "\tNome: " + channel.name + " | " + (channel.topic ? "Topico: " + channel.topic : "") + "\n";
		socket.write(linha)
	}
}
