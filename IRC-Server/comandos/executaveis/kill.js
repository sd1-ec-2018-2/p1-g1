let comandoPassado;
module.exports = new function () {
	this.command = "kill";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiUmArgumento(socket, args, ircServer))
		return;

	let nick = args[1];
	let motivo = args.join(" ").split(":")[1];
	motivo = (motivo ? motivo : "");
	motivo = motivo.replace(/\s{2,}/, " ");
	motivo = motivo === " " ? "" : motivo;

	if (!ircServer.isServerOperator(socket))
		return ircServer.sendMessageBack(socket, "ERRO: somente operadores do servidor podem executar esse comando");

	let targetSocket = ircServer.findSocketByNick(nick);

	if (!targetSocket)
		return ircServer.sendMessageBack(socket, "ERRO: alvo nao encontrado");

	if (ircServer.isServerOperator(targetSocket))
		return ircServer.sendMessageBack(socket, "ERRO: nao e possivel usar esse comando em outro operador");

	ircServer.sendMessageBack(targetSocket, "Desconectado pelo operador <" + socket.nick + "> " + (motivo ? "com o motivo: " + motivo : "sem motivo"))
	ircServer.addNickOnBlackList(targetSocket.nick);
	targetSocket.end();
}