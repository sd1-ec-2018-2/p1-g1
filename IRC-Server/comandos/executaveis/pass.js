let comandoPassado
module.exports = new function () {
	this.command = "pass"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim()
		if (comandoPassado.toLowerCase() != this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");
	if (!validacoes.possuiUmArgumento(socket, args, ircServer)) return;

	if (socket.logged || !ircServer.hasPassword()) {
		ircServer.sendMessageBack(socket, "Você já está logado");
		return
	}

	let passTyped = args[1].trim();

	if (passTyped === ircServer.getPassword()) {
		socket.logged = true;
	} else {
		ircServer.sendMessageBack(socket, "ERRO: senha inválida")
	}
}
