let comandoPassado;
module.exports = new function () {
	this.command = "version";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	ircServer.sendMessageBack(socket, ircServer.getVersion())
}