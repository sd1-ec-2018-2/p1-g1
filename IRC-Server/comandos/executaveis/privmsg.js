let comandoPassado;
module.exports = new function () {
	this.command = "privmsg"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer)
		|| !validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	let target = args[1].trim();
	let msg = args.join(' ').split(':')[1];

	if (!msg) {
		let message = "sintaxe inválida! (privmsg <target> :msg)";
		ircServer.sendMessageBack(socket, message)
		return
	}
	sendMessage(msg, socket, target, ircServer);
}

function sendMessage(message, fromSocket, toTarget, ircServer) {
	toTarget = toTarget.trim();
	message = "<" + fromSocket.nick + ">: " + message;
	if (toTarget.startsWith("#") || toTarget.startsWith("&")) {
		let channel = ircServer.findChannelByName(toTarget);

		if (!channel) return ircServer.sendMessageBack(fromSocket, "ERRO: Canal inexistente!");

		if (!channel.isMember(fromSocket) && !ircServer.isServerOperator(fromSocket)) {
			return ircServer.sendMessageBack(fromSocket, "ERRO: voce nao e membro desse canal");

		}
		channel.sendMessage(fromSocket, message);
	} else {
		let socketFinded = ircServer.findSocketByNick(toTarget);
		if (socketFinded) {
			socketFinded.write(ircServer.getMoment() + message);
			return
		}
		message = "<Servidor>: usuário não encontrado\n";
		ircServer.sendMessageBack(fromSocket, message)
	}
}
