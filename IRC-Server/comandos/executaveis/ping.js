let comandoPassado
let time;
module.exports = new function () {
	this.command = "ping"
	this.execute = function (args, socket, ircServer) {
		time = Date.now();
		comandoPassado = args[0].trim()
		if (comandoPassado.toLowerCase() != this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
}

function run(args, socket, ircServer) {
	if (args.length < 2) {
		let message = socket.localAddress + ":" + socket.localPort;
		message = "PONG: " + message + "\n";
		message += "tempo de processamento: " + (Date.now() - socket.ultmaInteracao) + " ms";
		ircServer.sendMessageBack(socket, message);
		return
	}

	let socketTarget = ircServer.findSocketByNick(args[1]);

	if (socketTarget) {
		let message = socketTarget.remoteAddress + ":" + socketTarget.remotePort;
		message = "PING: " + message + "\n";
		message += "tempo de processamento: " + (Date.now() - time) + " ms";
		ircServer.sendMessageBack(socket, message);
	} else {
		ircServer.sendMessageBack(socket, "PONG: Alvo não encotrado");
	}
}

