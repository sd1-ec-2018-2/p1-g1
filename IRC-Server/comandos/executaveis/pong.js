let comandoPassado
module.exports = new function () {
	this.command = "pong"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim()
		if (comandoPassado.toLowerCase() != this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
}

function run(args, socket, ircServer) {
	/*
	* metodo em branco, porque quando o user usa o pong
	* ele serve apenas para zerar o contador de inatividade
	* */
}
