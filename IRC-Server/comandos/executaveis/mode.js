let comandoPassado;
module.exports = new function () {
	this.command = "mode";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	if (args[2].length > 2)
		return ircServer.sendMessageBack(socket, "ERRO: nao e permitido utilizar mais de um modificador no mesmo comando!");

	let pattern = /^(\-|\+)([a-zA-Z])/;
	let target = args[1];
	let tipo;
	let action;
	let match = pattern.exec(args[2]);

	if (!match) {
		ircServer.sendMessageBack(socket, "ERRO: Parametros inválidos");
		return
	}

	action = match[1];
	tipo = match[2];

	if (target.startsWith("#"))
		runChannelMode(action, tipo, socket, args, ircServer);
	else runUserMode(action, tipo, socket, target, ircServer)

}

function runChannelMode(action, tipo, socket, args, ircServer) {
	let channel = ircServer.findChannelByName(args[1]);

	if (!channel)
		return ircServer.sendMessageBack(socket, "ERRO: canal nao existe");

	if (!channel.isOperator(socket) && !ircServer.isServerOperator(socket))
		return ircServer.sendMessageBack(socket, "Voce nao possui privilegios de operador");

	let adicionar = action === "+";

	switch (tipo) {
		case"o":
			let fulano = ircServer.findSocketByNick(args[3]);
			if (!fulano)
				return ircServer.sendMessageBack(socket, "ERRO: usuario nao encontrado");

			if (!channel.isMember(fulano))
				return ircServer.sendMessageBack(socket, "ERRO: Usuario nao e membro do canal");

			if (adicionar)
				return channel.addOperator(fulano);

			channel.rmOperator(fulano);
			break;
		case"v":
			let voicer = ircServer.findSocketByNick(args[3]);
			if (!voicer)
				return ircServer.sendMessageBack(socket, "ERRO: usuario nao encontrado");

			if (!channel.isMember(voicer))
				return ircServer.sendMessageBack(socket, "ERRO: Usuario nao e membro do canal");

			if (adicionar)
				return channel.addVoicer(voicer);

			channel.rmVoicer(voicer);
			break;
		case"l":
			let maxClients = Number(args[3]);

			if (Number.isNaN(maxClients))
				return ircServer.sendMessageBack(socket, "ERRO: O numero maximo deve ser um numero");

			if (maxClients <= 0)
				return ircServer.sendMessageBack(socket, "ERRO: O numero maximo deve ser maior que zero");

			if (adicionar)
				return channel.setMaxClients(maxClients);
			channel.maxClients = undefined;
			break;
		case"k":
			let hasQ = channel.modes.includes("q");
			if (adicionar) {
				channel.key = args[3];
				return !hasQ ? channel.sendMessage(socket, "Senha Alterada =>" + args[3]) : undefined;
			}
			channel.key = "";
			return !hasQ ? channel.sendMessage(socket, "Senha removida") : undefined;
		case"i":
		case"m":
		case"n":
		case"q":
		case"t":
			if (!channel.modes.includes(tipo) && adicionar)
				return channel.modes.push(tipo);

			if (!adicionar) {
				channel.modes.splice(channel.indexOf(tipo), 1);
				if (tipo === "i") {
					channel.addAllMembers(channel.invited);
					channel.removeAllInvited();
				}
				if (tipo === "m")
					channel.voicers = [];
			}
			break;
		default:
			return ircServer.sendMessageBack(socket, 'ERRO: Modificador inválido!')
	}
}

function runUserMode(action, tipo, socket, targetNick, ircServer) {
	let adicionar = action === "+";
	if (socket.nick !== targetNick)
		return ircServer.sendMessageBack(socket, "ERRO: So e possivel alterar o modo para o seu proprio nickname");

	switch (tipo) {
		case"i":
			if (!socket.userMode.includes("i") && adicionar)
				return socket.userMode.push("i");

			if (!adicionar)
				socket.userMode.splice(socket.userMode.indexOf("i"), 1);
			break;
		case"o":
			if (adicionar)
				return ircServer.sendMessageBack(socket, "ERRO: para essa operação use o comando OPER");

			socket.userMode = socket.userMode.replace(tipo, "");
			break;
		default:
			return ircServer.sendMessageBack(socket, 'ERRO: Modificador inválido!')
	}
}

/**
 * Agradecimentos ao grupo ec-2017-2/p1-g3, pois a ideia de usar um switchCase foi
 * fortemente inspirado na implementaçao deles
 * */