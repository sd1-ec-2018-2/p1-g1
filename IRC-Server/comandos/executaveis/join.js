let comandoPassado;
module.exports = new function () {
	this.command = "join"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
};

function run(args, socket, ircServer) {

	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiUmArgumento(socket, args, ircServer))
		return;

	processArgs(args, socket, ircServer);

}


function processArgs(args, socket, ircServer) {

	let canais = args[1].trim().split(",")

	let chaves = []

	if (args[2]) {
		chaves = args[2].trim().split(",")
	}

	if (canais[0] === 0) {
		let part = require("./part.js");
		let params = ["part", "*"];
		part.execute(params, socket, ircServer)
		return
	}

	if (canais.length === 1) {
		joinChannel(ircServer, canais[0], chaves[0], socket)
		return
	}

	canais.forEach(function (nomeCanal, i) {
		joinChannel(ircServer, nomeCanal, chaves[i], socket)
	})
}

function joinChannel(ircServer, nomeCanal, chave, socket) {
	chave = chave ? chave.trim() : "";

	if (!nomeCanal.startsWith("#") && !nomeCanal.startsWith("&")) {
		let message = "ERRO: nome do canal inválido! (#nomeCanal ou &nomeCanal)";
		ircServer.sendMessageBack(socket, message);
		return
	}
	let channelFinded = ircServer.findChannelByName(nomeCanal);
	if (channelFinded) {
		channelFinded.addMember(socket, chave)
	} else {
		let Channel = require("../../channel/channel");
		let channel = new Channel(nomeCanal, socket, chave, undefined);
		ircServer.addChannel(channel)
	}
}

