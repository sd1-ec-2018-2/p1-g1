let comandHelper = {
	obtenhaTodosSocketsDesconhecidos: obtenhaTodosSocketsDesconhecidos,
	obtenhaSocketsDesconhecidosPorMascara: obtenhaSocketsDesconhecidosPorMascara,
	findChannelsByMask: findChannelsByMask,
	findVisibleSocketsByMask: findVisibleSocketsByMask,
};

function obtenhaTodosSocketsDesconhecidos(ircServer, socketExecutante) {
	return obtenhaSocketsDesconhecidosPorMascara(ircServer, socketExecutante, "")
}

function obtenhaSocketsDesconhecidosPorMascara(ircServer, socketExecutante, pattern) {
	let canaisDoExecutante = ircServer.findAllChannelsOfNick(socketExecutante.nick);
	let visibleSockets = findVisibleSocketsByMask(ircServer, socketExecutante, pattern);
	if (canaisDoExecutante === undefined || canaisDoExecutante.length === 0) {
		return visibleSockets;
	}

	return visibleSockets.filter(function (socketDesconhecido) {
		let canaisSocketDesconhecido = ircServer.findAllChannelsOfNick(socketDesconhecido.nick);
		return canaisDoExecutante.some(function (canalDoExecutante) {
			return !canaisSocketDesconhecido.includes(canalDoExecutante)
		})
	});

}

function findVisibleSocketsByMask(ircServer, socketExecutante, pattern) {
	let regex = getRegexMask(pattern);
	return ircServer.getAllClients().filter(function (socketDesconhecido) {
		return (regex.test(socketDesconhecido.nick) ||
			regex.test(socketDesconhecido.realName)) &&
			ircServer.isUserVisible(socketDesconhecido) &&
			socketDesconhecido !== socketExecutante
	})
}


function getRegexMask(pattern) {
	let regex = "^" + pattern.replace(/\*/, ".*")
		.replace(/\.+/, ".").replace("#", "");
	return RegExp(regex, "i");
}

function findChannelsByMask(ircServer, pattern) {
	let regex = getRegexMask(pattern);
	return ircServer.channels.filter(function (channel) {
		return regex.test(channel.name.replace("#", ""));
	})
}

module.exports = comandHelper;
