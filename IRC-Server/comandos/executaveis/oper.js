let comandoPassado;
module.exports = new function () {
	this.command = "oper";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	let operadoresPersistidos = ircServer.getOperadoresPersistidos();

	let finded = operadoresPersistidos.filter(function (user) {
		return (args[1] === user.username) && (args[2] === user.password)
	});

	if (finded.length !== 1) {
		ircServer.sendMessageBack(socket, "ERRO: login/password invalido(s)");
		return;
	}

	socket.userMode.push("o");
	ircServer.sendMessageBack(socket, "Agora voce e um operador");
}
