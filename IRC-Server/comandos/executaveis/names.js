let comandoPassado
module.exports = new function () {
	this.command = "names"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim()
		if (comandoPassado.toLowerCase() != this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
}

function run(args, socket, ircServer) {
	if (args[1]) {
		let canais = args[1].trim().split(",")
		let channelsList = []
		canais.forEach(function (nomeCanal) {
			let channel = ircServer.findChannelByName(nomeCanal)
			if (channel) {
				channelsList.push(channel)
			}
		})
		printMembers(channelsList, socket, ircServer);
		return
	}
	printMembers(ircServer.channels, socket, ircServer)
}

function printMembers(channelsList, socket, ircServer) {
	channelsList.forEach(function (channel) {
		let nomeCanal = channel.name + "\n"
		let members = channel.users.reduce(function (strAcumulator, socketMember) {
			if (!ircServer.isUserVisible(socketMember)) {
				return strAcumulator
			}
			return strAcumulator + "\t" + socketMember.nick + "\n"
		}, "")
		socket.write(nomeCanal + members + "\n")
	})
}
