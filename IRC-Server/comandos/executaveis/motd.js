let comandoPassado;
module.exports = new function () {
	this.command = "motd";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

//Implementação do servidor TARGET omitida (to-do), pois no projeto utlizaremos apenas nosso próprio servidor IRC.

function run(args, socket, ircServer) {
	ircServer.sendMessageBack(socket, ircServer.getMotd())
}
