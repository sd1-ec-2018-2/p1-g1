let comandoPassado;
module.exports = new function () {
	this.command = "quit";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	quit(args, socket, ircServer);
}

function quit(args, socket, ircServer) {
	let mensagem = args.join(' ').split(':')[1];
	if (mensagem) {
		mensagem = "<" + socket.nick + "> " + "saiu, com a mensagem: " + mensagem
	} else {
		mensagem = "<" + socket.nick + "> " + "saiu"
	}

	if (ircServer.isUserVisible(socket)) {
		ircServer.broadcast(mensagem, socket);
	}

	socket.end();
}
