let comandoPassado;
module.exports = new function () {
	this.command = "kick";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	let nickNames = args[2].split(",");
	let channelsNames = args[1].split(",");
	let reacao = args.join(" ").split(":")[1];
	reacao = "[kicked]" + (reacao ? reacao : "");

	let candidateChannels = channelsNames.map(function (nameChannel) {
		return ircServer.findChannelByName(nameChannel)
	});

	let channels = candidateChannels.filter(function (channel) {
		if (channel)
			return channel.isOperator(socket) || ircServer.isServerOperator(socket);
	});

	if (channels.length === 0)
		return ircServer.sendMessageBack(socket, "ERRO: voce nao tem privilegios de operador ou esse canal nao existe");

	if (candidateChannels.length !== channels.length)
		ircServer.sendMessageBack(socket, "voce nao tem privilegios de operador em um ou mais canais");

	args[1] = channels.reduce(function (acumulator, channel) {
		return acumulator + "," + channel.name;
	}, "");

	let part = require("./part");

	args[1] = args[1].replace(/^,/, "");

	nickNames.forEach(function (nickName) {
		let argsPart = ["part", args[1], ":" + reacao];
		let socketToPart = ircServer.findSocketByNick(nickName);
		if (socketToPart) {
			part.execute(argsPart, socketToPart, ircServer);
			ircServer.sendMessageBack(socketToPart, socket.nick + " removeu voce dos seguintes canais: " + args[1]);
			return
		}
		ircServer.sendMessageBack(socket, "ERRO: Usuario " + nickName + " nao encontrado!");

	})
}