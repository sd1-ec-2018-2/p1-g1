let comandoPassado

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

module.exports = new function () {
	this.command = "user"
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim()
		if (comandoPassado.toLowerCase() != this.command) {
			this.executed = false
			return
		}
		run(args, socket, ircServer)
		this.executed = true
	}
}

function run(args, socket, ircServer) {
	if (!socket.nick) {
		let message = "ERRO: Crie um nick antes de registrar como usuário! (NICK nick)"
		ircServer.sendMessageBack(socket, message);
		return
	}

	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.possuiQuatroArgumentos(socket, args, ircServer)) return;

	cadastraUsuario(args, socket, ircServer);

}

function cadastraUsuario(args, socket, ircServer) {

	let userName = args[1].trim();
	let socketFinded = ircServer.findSocketByUserName(userName)

	if (socketFinded && socketFinded !== socket) {
		let message = "ERRO: Já existe esse userName!";
		ircServer.sendMessageBack(socket, message)
		return
	}

	if (Number.isNaN(Number(args[2]))) {
		let message = "ERRO: modo inválido!";
		ircServer.sendMessageBack(socket, message);
		return
	}

	let modeBitMask = Number(args[2]).toString(2);

	if (modeBitMask.length >= 4 && modeBitMask.charAt(modeBitMask.length - 4) === '1') {
		socket.userMode += "i"
	}

	socket.userMode = socket.userMode === undefined ? [] : socket.userMode;

	let realName = args.join(' ').split(':')[1].trim();

	realName = realName.split(" ")
		.reduce(function (acu, str) {
			return acu + " " + str.capitalize()
		}, "").trim();

	socket.user = userName;
	socket.realName = realName;
	socket.identifier = socket.nick + "!" + userName + "@" + socket.remoteAddress;

	if (!socket.joined) {
		socket.joined = Date.now()
	}
	let motd = require("./motd");
	motd.execute(["motd"], socket, ircServer);
}
