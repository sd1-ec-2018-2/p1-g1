let comandoPassado;
module.exports = new function () {
	this.command = "time";
	this.executed = false;
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	//todo - segundo a rfc esse comando pode consultar o time em outros servidores, porem aqui trabalhamos somete com um, entao nao precisade paramns
	socket.write(ircServer.getMoment() + "\n")
}
