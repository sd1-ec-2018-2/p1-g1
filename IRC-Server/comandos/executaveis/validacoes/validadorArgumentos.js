let msgArgumento = "Faltam argumentos!";
let validacoes = {
	isUserRegistrado: isUserRegistrado,
	possuiUmArgumento: possuiUmArgumento,
	possuiDoisArgumentos: possuiDoisArgumentos,
	possuiQuatroArgumentos: possuiQuatroArgumentos,
	isValidNickName: isValidNickName,
};

function isUserRegistrado(socket, ircServer) {
	if (!validaExistenciaArgumento(socket.identifier)) {
		ircServer.sendMessageBack(socket, "ERRO: Registre-se antes de usar esse comando! (User <username> <usermode> <unused> :realName)");
		return false
	}
	return true;
}

function possuiUmArgumento(socket, args, ircServer) {
	if (!validaExistenciaArgumento(args[1])) {
		ircServer.sendMessageBack(socket, msgArgumento);
		return false;
	}
	return true;
}

function possuiDoisArgumentos(socket, args, ircServer) {
	let validation = validaExistenciaArgumento(args[1]) &&
		validaExistenciaArgumento(args[2]);

	if (!validation) {
		ircServer.sendMessageBack(socket, msgArgumento);
		return false;
	}
	return true;
}


function possuiQuatroArgumentos(socket, args, ircServer) {
	let validation = validaExistenciaArgumento(args[1]) &&
		validaExistenciaArgumento(args[2]) &&
		validaExistenciaArgumento(args[3]) &&
		validaExistenciaArgumento(args[4]);

	if (!validation) {
		ircServer.sendMessageBack(socket, msgArgumento);
		return false;
	}
	return true;
}

function validaExistenciaArgumento(argumento) {
	return argumento !== undefined && argumento !== "";
}

function isValidNickName(socket, nick, ircServer) {
	let pattern = /[^a-zA-Z\._\d]+/;
	if (pattern.test(nick.trim())) {
		ircServer.sendMessageBack(socket, "ERRO: nickname inválido!");
		return false;
	}
	return true;
}

module.exports = validacoes;

