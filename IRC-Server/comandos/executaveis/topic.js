let comandoPassado
module.exports = new function () {
	this.command = "topic";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
}

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.possuiUmArgumento(socket, args, ircServer))
		return;

	let channel = ircServer.findChannelByName(args[1]);

	if (!channel)
		return ircServer.sendMessageBack(socket, "ERRO: canal nao encontrado");

	if (args.length === 2)
		return ircServer.sendMessageBack(socket, "Topic: " + (channel.topic ? channel.topic : ""));

	if (!validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	if (!channel.isOperator(socket) && !ircServer.isServerOperator(socket))
		return ircServer.sendMessageBack(socket, "ERRO: você não é operador");

	let topic = args.join(' ').split(':')[1];

	channel.setTopic(topic)
}