let comandoPassado
module.exports = new function () {
	this.command = "invite";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {
	let validacoes = require("./validacoes/validadorArgumentos");

	if (!validacoes.isUserRegistrado(socket, ircServer) ||
		!validacoes.possuiDoisArgumentos(socket, args, ircServer))
		return;

	let invitedSocket = ircServer.findSocketByNick(args[1]);

	if (!invitedSocket)
		return ircServer.sendMessageBack(socket, "ERRO: usuario nao encontrado!");

	let invitedChannel = ircServer.findChannelByName(args[2]);

	if (!invitedChannel)
		return ircServer.sendMessageBack(socket, "ERRO: canal inexistente");

	if (invitedChannel.isInvitedOnly() && !(invitedChannel.isOperator(socket) || ircServer.isServerOperator(socket)))
		return ircServer.sendMessageBack(socket, "ERRO: Somente Operadores podem convindar para esse canal");

	if (invitedChannel.isInvited(socket))
		return ircServer.sendMessageBack(socket, "ERRO: Usuario ja foi convidado a esse canal");

	if (!invitedChannel.isMember(socket))
		return ircServer.sendMessageBack(socket, "ERRO: voce nao e membro desse canal");

	invitedChannel.addInvited(invitedSocket);

	let msg = "Usuario <" + invitedSocket.nick + "> convidado para o canal";

	if (invitedChannel.isInvitedOnly()) {
		ircServer.sendMessageBack(socket, msg);
		let argumentos = ["privmsg", args[1], ":" + msg];
		let privmsg = require("./privmsg");
		privmsg.execute(argumentos, socket, ircServer);
		return
	}

	invitedChannel.sendMessage(socket, msg);
}
