let comandoPassado;
module.exports = new function () {
	this.command = "lusers";
	this.execute = function (args, socket, ircServer) {
		comandoPassado = args[0].trim();
		if (comandoPassado.toLowerCase() !== this.command) {
			this.executed = false;
			return
		}
		run(args, socket, ircServer);
		this.executed = true
	}
};

function run(args, socket, ircServer) {

	let allClientes = ircServer.getAllClients();
	let totalInvisiveis = allClientes.reduce(function (acumulator, clientSocket) {
		return acumulator + (ircServer.isUserVisible(clientSocket) ? 0 : 1)
	}, 0);

	let totalServerOperators = ircServer.findServerOperators().length;
	let totalClientes = ircServer.getTotalClients();
	let totalClientesRegistrados = allClientes.length;
	let clientesDesconhecidos = totalClientes - totalClientesRegistrados;
	let totalCanais = ircServer.channels.length;

	ircServer.sendMessageBack(socket, "Inicio lista lusers");
	socket.write(`\tExiste(m) ${totalClientes} usuário(s), sendo ${totalInvisiveis} invisível(is), em 1 servidor\n`);
	socket.write(`\tOperadores online: ${totalServerOperators}\n`);
	socket.write(`\tConexões desconhecidas: ${clientesDesconhecidos}\n`);
	socket.write(`\tCanais criados: ${totalCanais}\n`);
	ircServer.sendMessageBack(socket, "Fim lista lusers");

}