let comandosExecutados = {};
module.exports = new function () {
	let comandList = []
	comandList.push(require("./executaveis/nick.js"))
	comandList.push(require("./executaveis/user.js"))
	comandList.push(require("./executaveis/pass.js"))
	comandList.push(require("./executaveis/join.js"))
	comandList.push(require("./executaveis/who.js"))
	comandList.push(require("./executaveis/whois.js"))
	comandList.push(require("./executaveis/part.js"))
	comandList.push(require("./executaveis/quit.js"))
	comandList.push(require("./executaveis/topic.js"))
	comandList.push(require("./executaveis/names.js"))
	comandList.push(require("./executaveis/list.js"))
	comandList.push(require("./executaveis/invite.js"))
	comandList.push(require("./executaveis/privmsg.js"))
	comandList.push(require("./executaveis/ping.js"))
	comandList.push(require("./executaveis/pong.js"))
	comandList.push(require("./executaveis/motd.js"))
	comandList.push(require("./executaveis/stats.js"))
	comandList.push(require("./executaveis/mode.js"))
	comandList.push(require("./executaveis/time.js"))
	comandList.push(require("./executaveis/oper.js"))
	comandList.push(require("./executaveis/kick.js"))
	comandList.push(require("./executaveis/kill.js"))
	comandList.push(require("./executaveis/lusers.js"))
	comandList.push(require("./executaveis/version.js"))
	comandList.push(require("./executaveis/comandoNaoSuportado.js"))// O comando não suportado sempre deve ser o ultimo da lista.

	this.execute = function (data, socket, ircServer) {
		let args = String(data).trim().split(/\s+/);
		socket.ultmaInteracao = Date.now();
		for (let i = 0; i < comandList.length; i++) {
			comandList[i].execute(args, socket, ircServer);
			if (comandList[i].executed) {
				if (comandosExecutados[comandList[i].command]) {
					comandosExecutados[comandList[i].command]++;
				} else {
					comandosExecutados[comandList[i].command] = 1;
				}
				ircServer.estatisticas["comandosExecutados"] = comandosExecutados;
				break
			}
		}
	}
};
